<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\FilterPlugin\Indexer;

use Doctrine\ORM\EntityManager;
use Omni\Sylius\FilterPlugin\Doctrine\ORM\ProductAttributeRepository;
use Omni\Sylius\FilterPlugin\Model\SearchTagAwareInterface;
use Sylius\Component\Core\Model\ProductInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OrmIndexer implements IndexerInterface
{
    const TAG_SEPARATOR = '|';

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var ProductAttributeRepository
     */
    private $attributeRepository;

    /**
     * @var string
     */
    private $class;

    /**
     * @var TagManager
     */
    private $tagManager;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * OrmIndexer constructor.
     *
     * @param EntityManager              $entityManager
     * @param ProductAttributeRepository $attributeRepository
     * @param string                     $class
     * @param TagManager                 $tagManager
     *
     * @throws \Exception
     */
    public function __construct(
        EntityManager $entityManager,
        ProductAttributeRepository $attributeRepository,
        string $class,
        TagManager $tagManager
    ) {
        $this->tagManager = $tagManager;
        $this->attributeRepository = $attributeRepository;
        $this->em = $entityManager;
        $this->class = $class;

        if (!in_array('Omni\Sylius\FilterPlugin\Model\SearchTagAwareInterface', class_implements($this->class))) {
            throw new \Exception(sprintf(
                'Class\'%s\' must implement \'%s\' interface.',
                $this->class,
                'Omni\Sylius\FilterPlugin\Model\SearchTagAwareInterface'
            ));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function populate()
    {
        $filterableAttributes = $this->attributeRepository->getFilterableAttributes();

        $products = [];
        $query = $this->em->createQuery("SELECT p FROM {$this->class} p ORDER BY p.id ASC");
        $iterator = $query->iterate();
        $i = 0;

        foreach ($iterator as $row) {
            /** @var SearchTagAwareInterface|ProductInterface $element */
            $element = $row[0];

            $data = $this->tagManager->extractTags($element, $filterableAttributes);
            $element->setTagIndex($this->tagManager->buildTagIndex($data));

            $this->em->persist($element);

            if ((++$i % 10) === 0) {
                $this->em->flush();
                $this->em->clear();
            }
        }

        $this->em->flush();

        $this->output && $this->output->writeln('');
        $this->output && $this->output->writeln(sprintf('Index updated successfully (%d items indexed)', $i));

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;

        return $this;
    }
}

<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\FilterPlugin\DependencyInjection;

use Omni\Sylius\FilterPlugin\Grid\Controller\ChoicesProvider;
use Sylius\Bundle\ResourceBundle\SyliusResourceBundle;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('omni_sylius_filter');

        $rootNode
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('attribute_choice_formation_strategy')
                    ->defaultValue(ChoicesProvider::CHOICE_FORMATION_STRATEGY_RESTRICTIVE)
                ->end()
                ->booleanNode('default_slider')->defaultTrue()->end()
                ->booleanNode('show_products_amount')->defaultTrue()->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}

<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\FilterPlugin\Grid\Controller;

use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Sylius\Bundle\ResourceBundle\Controller\ParametersParser;
use Sylius\Bundle\ResourceBundle\Controller\RequestConfiguration;
use Sylius\Component\Channel\Context\ChannelContextInterface;
use Sylius\Component\Channel\Model\ChannelInterface;
use Sylius\Component\Core\Repository\ProductRepositoryInterface;
use Sylius\Component\Grid\Provider\GridProviderInterface;
use Sylius\Component\Locale\Context\LocaleContextInterface;
use Symfony\Component\HttpFoundation\Request;

class ChoicesProvider
{
    const CHOICE_FORMATION_STRATEGY_STATIC = 'static';
    const CHOICE_FORMATION_STRATEGY_RESTRICTIVE = 'restrictive';
    const CHOICE_FORMATION_STRATEGY_INTUITIVE = 'intuitive';

    /**
     * @var GridProviderInterface
     */
    private $gridProvider;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var ParametersParser
     */
    private $parametersParser;

    /**
     * @var ChannelInterface
     */
    private $channel;

    /**
     * @var string
     */
    private $currentLocale;

    /**
     * @param RequestConfiguration $requestConfiguration
     */
    private $requestConfiguration;

    /**
     * @var string
     */
    private $choiceFormationStrategy = self::CHOICE_FORMATION_STRATEGY_STATIC;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @param GridProviderInterface $gridProvider
     * @param ProductRepositoryInterface $productRepository
     * @param ParametersParser $parametersParser
     * @param ChannelContextInterface $channelContext
     */
    public function __construct(
        GridProviderInterface $gridProvider,
        ProductRepositoryInterface $productRepository,
        ParametersParser $parametersParser,
        ChannelContextInterface $channelContext,
        LocaleContextInterface $localeContext
    ) {
        $this->gridProvider = $gridProvider;
        $this->productRepository = $productRepository;
        $this->parametersParser = $parametersParser;
        $this->channel = $channelContext->getChannel();
        $this->currentLocale = $localeContext->getLocaleCode();
    }

    /**
     * @param RequestConfiguration $requestConfiguration
     */
    public function setRequestConfiguration(RequestConfiguration $requestConfiguration)
    {
        $this->requestConfiguration = $requestConfiguration;
    }

    /**
     * @return string
     */
    public function getChoiceFormationStrategy(): string
    {
        return $this->choiceFormationStrategy;
    }

    /**
     * @param string $choiceFormationStrategy
     *
     * @throws \Exception
     */
    public function setChoiceFormationStrategy(string $choiceFormationStrategy): void
    {
        if (!in_array($choiceFormationStrategy, $this->getPossibleStrategies())) {
            throw new \Exception('Invalid choice formation strategy provided');
        }

        $this->choiceFormationStrategy = $choiceFormationStrategy;
    }

    /**
     * @return array
     */
    public function getPossibleStrategies(): array
    {
        return [
            self::CHOICE_FORMATION_STRATEGY_STATIC,
            self::CHOICE_FORMATION_STRATEGY_RESTRICTIVE,
            self::CHOICE_FORMATION_STRATEGY_INTUITIVE,
        ];
    }

    /**
     * Forms choices for filter form selection. The structure is as follows:
     *
     * choices = [
     *   'simple_attribute_1' => [
     *      'option a' => 3,  # here 3 is the amount of products containing the option
     *      'option b' => 2,
     *      # ...
     *   ],
     *   'selectable_attribute_1' => [ # select type attributes have different structure
     *      'option a' => [
     *         '_label' => 'Option A',
     *         '_amount' => 3,
     *      ],
     *      # ...
     *   ],
     *   'attribute_2' => [
     *     # ...
     *   ],
     *   # ...
     * ]
     *
     * @return array
     */
    public function getChoices()
    {
        // Get QueryBuilder for current taxon page
        $qb = $this->getAttributeChoicesQueryBuilder();

        if ($this->choiceFormationStrategy == self::CHOICE_FORMATION_STRATEGY_RESTRICTIVE) {
            $this->restrictOnSelectedAttributes($qb);
        }

        $choices = $this->processAttributes($qb->getQuery()->getArrayResult());

        if ($this->choiceFormationStrategy == self::CHOICE_FORMATION_STRATEGY_INTUITIVE) {
            $choices = $this->formIntuitiveChoices($choices);
        }

        return $choices;
    }

    /**
     * This method adds zero choices and makes sure that the amounts of choices are not altered
     * within the choices of the selected attribute.
     *
     * @param array $initialChoices
     *
     * @return array
     */
    protected function formIntuitiveChoices(array $initialChoices): array
    {
        $selectedAttributes = $this->getRequestedAttributes();

        // intuitive choices formation only makes sense if there are attributes selected. This improves performance.
        if (empty($selectedAttributes)) {
            return $initialChoices;
        }

        // we start with the fully restricted choices, that is all the requested attributes are added to restriction.
        $choicesArray = $this->processAttributes($this->getRestrictedAttributes());

        foreach ($choicesArray as $attributeCode => $choiceSection) {
            // if the attribute is selected we need it its choices to be excluded from restriction.
            // Lots of performance issues here, but hey! That's why this functionality is configurable :)
            // Use OmniSyliusElasticsearchPlugin if you have a problem with performance here.
            if (in_array($attributeCode, array_keys($selectedAttributes))) {
                $restrictedAttributes = $this->processAttributes($this->getRestrictedAttributes($attributeCode));
                $choiceSection = $restrictedAttributes[$attributeCode];
                $choicesArray[$attributeCode] = $choiceSection;
            }

            // next section adds zero choices
            if (!isset($initialChoices[$attributeCode])) {
                continue;
            }

            foreach (array_diff_key($initialChoices[$attributeCode], $choiceSection) as $missingChoice => $value) {
                $choicesArray[$attributeCode][$missingChoice] = $value;

                is_array($value) ? $choicesArray[$attributeCode][$missingChoice]['_amount'] = 0 :
                    $choicesArray[$attributeCode][$missingChoice] = 0;
            }
        }

        return $choicesArray;
    }

    /**
     * Gets the unformatted response from DB about the available choices with the restriction of selected
     * attributes.
     *
     * @param string $except   If specified, the returned choices will be provided as if the specified
     *                         attribute code was never set. This is used for the formation of selected
     *                         attribute option counts.
     *
     * @return array
     */
    protected function getRestrictedAttributes(string $except = null): array
    {
        $qb = $this->getAttributeChoicesQueryBuilder();
        $this->restrictOnSelectedAttributes($qb, $except);

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * Processes the supplied attribute data from that retrieved from DB to a wanted format.
     *
     * input:
     *
     *   [
     *     [
     *       count:   54,
     *       text:    "some value",
     *       boolean: null,
     *       json:    null,
     *       configuration: [],
     *       code: loading_weight
     *     ],
     *     [ ... ],
     *   ]
     *
     * output: { described in the annotations of getChoices() }
     *
     * @param array $attributeData
     *
     * @return array
     */
    protected function processAttributes(array $attributeData): array
    {
        $choices = [];

        foreach ($attributeData as $attributeValue) {
            $type = $this->getAttrubuteType($attributeValue);
            $code = $attributeValue['code'];
            $attributeValue['count'] = (int) $attributeValue['count'];

            if (!$type) {
                continue;
            }

            if ($type == 'json') {
                foreach ($attributeValue[$type] as $option) {
                    $choices[$code][$option]['_label'] =
                        $attributeValue['configuration']['choices'][$option][$this->currentLocale];

                    if (isset($choices[$code][$option]['_amount'])) {
                        $choices[$code][$option]['_amount'] += $attributeValue['count'];
                    } else {
                        $choices[$code][$option]['_amount'] = $attributeValue['count'];
                    }
                }

                continue;
            }

            $choices[$code][(string) $attributeValue[$type]] = $attributeValue['count'];
        }

        return $choices;
    }

    /**
     * @return int
     */
    public function getLowerPrice(): int
    {
        $qb = $this
            ->getPriceQueryBuilder()
            ->orderBy('ch.price', 'ASC')
        ;

        try {
            return $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            return 0;
        }
    }

    /**
     * @return int
     */
    public function getUpperPrice(): int
    {
        $qb = $this
            ->getPriceQueryBuilder()
            ->orderBy('ch.price', 'DESC')
        ;

        try {
            return $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            return 0;
        }
    }

    /**
     * @return Request
     */
    protected function getRequest()
    {
        if (!$this->request) {
            $this->request = $this->requestConfiguration->getRequest();
        }

        return $this->request;
    }

    /**
     * @return QueryBuilder
     */
    protected function getAttributeChoicesQueryBuilder(): QueryBuilder
    {
        $qb = $this->getQueryBuilder();

        $qb->resetDQLParts(['select', 'orderBy'])
            ->addSelect('count(o.id) AS count')
            ->addSelect('av.text')
            ->addSelect('av.boolean')
            ->addSelect('av.json')
            ->addSelect('attribute.configuration')
            ->addSelect('attribute.code')
            ->leftJoin('o.attributes', 'av')
            ->leftJoin('av.attribute', 'attribute')
            ->resetDQLPart('groupBy')
            ->andWhere('attribute.filterable = 1')
            ->addGroupBy('av.text')
            ->addGroupBy('av.boolean')
            ->addGroupBy('av.json')
            ->addGroupBy('attribute.configuration')
            ->addGroupBy('attribute.code')
        ;

        return $qb;
    }

    /**
     * @param array $atributeValue
     *
     * @return string
     */
    private function getAttrubuteType(array $atributeValue): ?string
    {
        foreach (['text', 'boolean', 'json'] as $type) {
            if ($atributeValue[$type] !== null) {
                return $type;
            }
        }

        return null;
    }

    /**
     * Makes sure that only products that have current selected attribute values from request
     * are taken into account.
     *
     * @param QueryBuilder $qb
     * @param string       $except
     */
    private function restrictOnSelectedAttributes(QueryBuilder $qb, string $except = null): void
    {
        foreach ($this->getRequestedAttributes() as $attribute => $values) {
            if (is_array($values) && $attribute != $except) {
                $count = 0;
                $orX = $qb->expr()->orX();

                foreach ($values as $value) {
                    $value = sprintf('%s%s:%s%s', '|', $attribute, (string)$value, '|');
                    $param = $attribute . '_' . $count++;

                    // Here options of the same attribute are handled with an OR operator
                    $orX = $orX->add($qb->expr()->like('o.tagIndex', ':' . $param));
                    $qb->setParameter($param, '%' . trim($value) . '%');
                }

                // Here options of different attributes are handled with an AND operator
                $qb->andWhere($orX);
            }
        }
    }

    /**
     * Returns the attributes and their values set in request query
     *
     * @return array
     */
    private function getRequestedAttributes(): array
    {
        $criteria = $this->getRequest()->query->get('criteria');

        return is_array($criteria) && isset($criteria['attributes']) ? $criteria['attributes'] : [];
    }

    /**
     * @return QueryBuilder
     */
    private function getPriceQueryBuilder(): QueryBuilder
    {
        $qb = $this->getQueryBuilder();

        return $qb->resetDQLParts(['select', 'orderBy'])
            ->addSelect('ch.price')
            ->innerJoin('o.variants', 'v1')
            ->innerJoin('v1.channelPricings', 'ch')
            ->andWhere('ch.channelCode = :code')
            ->setParameter('code', $this->channel->getCode())
            ->setMaxResults(1)
        ;
    }

    /**
     * @return QueryBuilder
     */
    private function getQueryBuilder(): QueryBuilder
    {
        $grid = $this->gridProvider->get($this->requestConfiguration->getGrid());

        $driverConfiguration = $grid->getDriverConfiguration();
        $request = $this->requestConfiguration->getRequest();

        $grid->setDriverConfiguration($this->parametersParser->parseRequestValues($driverConfiguration, $request));

        $configuration = $grid->getDriverConfiguration();

        if (isset($configuration['repository']['method'])) {
            $method = $configuration['repository']['method'];
            $arguments = isset($configuration['repository']['arguments']) ? array_values($configuration['repository']['arguments']) : [];

            $queryBuilder = $this->productRepository->$method(...$arguments);
        } else {
            $queryBuilder = $this->productRepository->createQueryBuilder('o');
        }

        return $queryBuilder;
    }
}

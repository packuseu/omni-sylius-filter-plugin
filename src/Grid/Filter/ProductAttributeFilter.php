<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\FilterPlugin\Grid\Filter;

use Doctrine\ORM\Query\Expr\Orx;
use Sylius\Component\Grid\Data\DataSourceInterface;
use Sylius\Component\Grid\Filtering\FilterInterface;

class ProductAttributeFilter implements FilterInterface
{
    /**
     * {@inheritdoc}
     */
    public function apply(DataSourceInterface $dataSource, string $name, $data, array $options): void
    {
        if (empty($data) || !is_array($data)) {
            return;
        }

        foreach ($data as $code => $values) {
            if (is_array($values)) {
                /** @var Orx $orX */
                $orX = $dataSource->getExpressionBuilder()->orX();

                foreach ($values as $value) {
                    $value = sprintf('%s%s:%s%s', '|', $code, (string)$value, '|');
                    $orX->add($dataSource->getExpressionBuilder()->like('o.tagIndex', '%'.trim($value).'%'));
                }

                $dataSource->restrict($dataSource->getExpressionBuilder()->andX($orX));
            }
        }
    }
}

<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\FilterPlugin\Grid\Filter;

use Omni\Sylius\CorePlugin\Doctrine\ORM\DataSource;
use Sylius\Component\Channel\Context\ChannelContextInterface;
use Sylius\Component\Grid\Data\DataSourceInterface;
use Sylius\Component\Grid\Filtering\FilterInterface;

class PriceFilter implements FilterInterface
{
    /**
     * @var ChannelContextInterface
     */
    protected $channelContext;

    /**
     * @param ChannelContextInterface $channelContext
     */
    public function __construct(ChannelContextInterface $channelContext)
    {
        $this->channelContext = $channelContext;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(DataSourceInterface $dataSource, string $name, $data, array $options): void
    {
        if (false === $this->isFilterNeeded($data)) {
            return;
        }

        /** @var DataSource $dataSource */
        $qb = $dataSource->getQueryBuilder();
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->between('chp.price', $data['lower'] * 100, $data['upper'] * 100));
        $andX->add($qb->expr()->eq('chp.channelCode', ':channelCode'));
        $qb->setParameter('channelCode', $this->channelContext->getChannel()->getCode());

        $qb
            ->join('o.variants', 'v1')
            ->join('v1.channelPricings', 'chp')
            ->andWhere($andX);
    }

    /**
     * @param $data
     * @return bool
     */
    private function isFilterNeeded(?array $data): bool
    {
        return is_array($data) && isset($data['lower']) && isset($data['upper']);
    }
}

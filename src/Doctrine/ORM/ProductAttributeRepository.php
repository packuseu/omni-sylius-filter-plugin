<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\FilterPlugin\Doctrine\ORM;

use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Component\Attribute\Model\AttributeInterface;

class ProductAttributeRepository extends EntityRepository
{
    /**
     * @return AttributeInterface[]
     */
    public function getFilterableAttributes()
    {
        $qb = $this->createQueryBuilder('a', 'a.code');

        return $qb
            ->andWhere($qb->expr()->eq('a.filterable', true))
            ->getQuery()
            ->getResult();
    }
}

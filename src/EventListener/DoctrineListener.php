<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\FilterPlugin\EventListener;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Omni\Sylius\FilterPlugin\Indexer\TagManager;
use Omni\Sylius\FilterPlugin\Model\SearchTagAwareInterface;
use Sylius\Component\Attribute\Model\Attribute;
use Sylius\Component\Product\Model\ProductAttributeValueInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class DoctrineListener
{
    /**
     * @var TagManager
     */
    private $tagManager;

    /**
     * @var RepositoryInterface
     */
    private $attributeRepository;

    /**
     * @var Attribute[]
     */
    private $filterableAttributes;

    public function __construct(TagManager $tagManager, RepositoryInterface $attributeRepository)
    {
        $this->tagManager = $tagManager;
        $this->attributeRepository = $attributeRepository;
    }

    public function preCreate(GenericEvent $event)
    {
        $this->reindex($event->getSubject());
    }

    public function preUpdate(GenericEvent $event)
    {
        $this->reindex($event->getSubject());
    }

    private function reindex(SearchTagAwareInterface $filterable)
    {
        $tags = $this->tagManager->extractTags($filterable, $this->getFilterableAttributes());
        $tags = $this->tagManager->buildTagIndex($tags);
        $filterable->setTagIndex($tags);
    }

    private function getFilterableAttributes()
    {
        if (null === $this->filterableAttributes) {
            /** @var Attribute[] $filterableAttributes */
            $this->filterableAttributes = $this->attributeRepository->getFilterableAttributes();
        }

        return $this->filterableAttributes;
    }
}
